﻿using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace ElevatorGame
{
    [System.Serializable]
    public class Elevator : IElevator
    {
        public enum Direction { NONE, UP, DOWN }
        private enum State { WAIT, MOVE, OPEN_DOOR, CLOSE_DOOR, STOP }

        #region Fields/Properties
        private List<int> m_pendingFloorsNoDir;
        private List<int> m_pendingFloorsUpDir;
        private List<int> m_pendingFloorsDownDir;
        private int m_goingToFloor;
        private Direction m_direction;
        private State m_state;
        private RectTransform m_elevatorView;
        #endregion

        #region Events
        public event UnityAction<int> goToFloorEvent;
        public event UnityAction stopEvent;
        #endregion

        #region Properties
        public float CurrentFloor { get { return (m_elevatorView.localPosition.y / m_elevatorView.rect.height) + 1; } }
        #endregion

        public Elevator(RectTransform elevatorView)
        {
            m_elevatorView = elevatorView;
            m_pendingFloorsNoDir = new List<int>();
            m_pendingFloorsUpDir = new List<int>();
            m_pendingFloorsDownDir = new List<int>();
        }

        public void WantMoveFromFloor(int floor, Direction direction)
        {
            AddPendingFloor(floor, direction);
            GoToNextFloor();
        }

        public void WantMoveToFloor(int floor)
        {
            AddPendingFloor(floor, Direction.NONE);
            GoToNextFloor();
        }

        public void Stop()
        {
            if(m_state == State.MOVE)
            {
                m_pendingFloorsNoDir = new List<int>();
                Wait();
                m_state = State.STOP;

                if(stopEvent != null)
                    stopEvent();
            }
        }

        public void OpenDoors()
        {
            m_state = State.OPEN_DOOR;
        }

        public void CloseDoors()
        {
            m_state = State.CLOSE_DOOR;
        }

        public void StopAtFloor(int floor)
        {
            m_pendingFloorsNoDir.Remove(floor);
            m_pendingFloorsUpDir.Remove(floor);
            m_pendingFloorsDownDir.Remove(floor);
            m_goingToFloor = 0;
            GoToNextFloor();
        }
        
        private void AddPendingFloor(int floor, Direction direction)
        {
            switch(direction)
            {
                case Direction.NONE:
                    if(!m_pendingFloorsNoDir.Contains(floor))
                        m_pendingFloorsNoDir.Add(floor);
                    break;
                case Direction.UP:
                    if(!m_pendingFloorsUpDir.Contains(floor))
                        m_pendingFloorsUpDir.Add(floor);
                    break;
                case Direction.DOWN:
                    if(!m_pendingFloorsDownDir.Contains(floor))
                        m_pendingFloorsDownDir.Add(floor);
                    break;
            }
        }

        private void GoToNextFloor()
        {
            int floorGoTo = GetNextFloorInDirection(m_direction);

            if(m_state == State.STOP)
            {
                m_direction = GetDirectionFromCurrentToFloor(floorGoTo);
                if(m_direction != Direction.NONE)
                {
                    m_state = State.MOVE;
                }

                floorGoTo = GetNextFloorInDirection(m_direction);
            }
            else if(floorGoTo == 0)
            {
                Wait();
                floorGoTo = GetNextFloorInDirection(m_direction);
            }

            if(floorGoTo > 0 && m_goingToFloor != floorGoTo)
            {
                m_direction = GetDirectionFromCurrentToFloor(floorGoTo);
                m_state = State.MOVE;
                m_goingToFloor = floorGoTo;
                if(goToFloorEvent != null)
                    goToFloorEvent(m_goingToFloor);
            }
        }

        private void Wait()
        {
            m_state = State.WAIT;
            m_direction = Direction.NONE;
        }

        private Direction GetDirectionFromCurrentToFloor(int floor)
        {
            if(floor <= 0)
                return Direction.NONE;
            else if(floor > CurrentFloor)
                return Direction.UP;
            else if(floor < CurrentFloor)
                return Direction.DOWN;
            else
                return Direction.NONE;
        }

        private int GetNextFloorInDirection(Direction direction)
        {
            int floorShouldGo = 0;

            if(direction == Direction.NONE)
            {
                int nearestNoDir = 0;
                int nearestUpDir = 0;
                int nearestDownDir = 0;

                nearestNoDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsNoDir, direction);

                if(m_state != State.STOP)
                {
                    nearestUpDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsUpDir, direction);
                    nearestDownDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsDownDir, direction);
                }

                floorShouldGo = GetNearestPendingFloorInDir(
                    new List<int>() { nearestNoDir, nearestUpDir, nearestDownDir }, direction);
            }
            else if(direction == Direction.UP)
            {
                int nearestNoDir = 0;
                int nearestUpDir = 0;

                nearestNoDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsNoDir, direction);

                if(m_state != State.STOP)
                {
                    nearestUpDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsUpDir, direction);
                }

                floorShouldGo = GetNearestPendingFloorInDir(
                    new List<int>() { nearestNoDir, nearestUpDir }, direction);
            }
            else
            {
                int nearestNoDir = 0;
                int nearestDownDir = 0;

                nearestNoDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsNoDir, direction);

                if(m_state != State.STOP)
                {
                    nearestDownDir = GetNearestPendingFloorInDir(
                        m_pendingFloorsDownDir, direction);
                }

                floorShouldGo = GetNearestPendingFloorInDir(
                    new List<int>() { nearestNoDir, nearestDownDir }, direction);
            }

            return floorShouldGo;
        }

        private int GetNearestPendingFloorInDir(List<int> pendingFloors, Direction direction)
        {
            int floorShouldGo = 0;

            if(pendingFloors.Count > 0)
            {
                foreach(int floor in pendingFloors)
                {
                    if(floor <= 0) continue;

                    bool isNewFloorCloser = false;

                    switch(direction)
                    {
                        case Direction.NONE:
                            isNewFloorCloser =
                                floorShouldGo == 0 || 
                                (Mathf.Abs(CurrentFloor - floor) <
                                Mathf.Abs(CurrentFloor - floorShouldGo));
                            break;
                        case Direction.UP:
                            isNewFloorCloser = 
                                floor > CurrentFloor && 
                                (floorShouldGo == 0 || floor < floorShouldGo);
                            break;
                        case Direction.DOWN:
                            isNewFloorCloser =
                                floor < CurrentFloor &&
                                (floorShouldGo == 0 || floor > floorShouldGo);
                            break;
                    }

                    if(isNewFloorCloser)
                    {
                        floorShouldGo = floor;
                    }
                }
            }

            return floorShouldGo;
        }
    }
}