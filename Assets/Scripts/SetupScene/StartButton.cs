﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ElevatorGame
{
    public class StartButton : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private GameObject m_errorText;
        #endregion

        public void OnClick()
        {
            bool isNumFloorsValid = Game.Instance.NumFloors > 0;

            m_errorText.SetActive(!isNumFloorsValid);

            if(isNumFloorsValid)
                SceneManager.LoadScene(Constants.Scenes.MAIN);
        }
    }
}