﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElevatorGame
{
    public class FloorsInputField : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private GameObject m_errorText;
        #endregion

        public void OnEndEdit(string str)
        {
            int numFloors = 0;
            System.Int32.TryParse(str, out numFloors);

            Game.Instance.NumFloors = numFloors;
            m_errorText.SetActive(numFloors <= 0);
        }
    }
}