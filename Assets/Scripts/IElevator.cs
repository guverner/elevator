﻿using UnityEngine.Events;

namespace ElevatorGame
{
    public interface IElevator
    {
        void WantMoveFromFloor(int floor, Elevator.Direction direction);
        void WantMoveToFloor(int floor);
        void Stop();
        void OpenDoors();
        void CloseDoors();
        void StopAtFloor(int floor);

        event UnityAction<int> goToFloorEvent;
        event UnityAction stopEvent;
    }
}