﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElevatorGame
{
    public abstract class SceneController : MonoBehaviour
    {
        public abstract void Init();
    }
}