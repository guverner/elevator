﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ElevatorGame
{
    public class Floor : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Text m_floorText;
        [SerializeField]
        private Button m_upButton;
        [SerializeField]
        private Button m_downButton;

        private int m_floorNumber;
        #endregion

        #region Public
        public void Init(int floorNumber, int numFloors, 
            UnityAction<int, Elevator.Direction> arrowButtonClickCallback)
        {
            m_floorNumber = floorNumber;

            m_floorText.text = floorNumber.ToString();

            m_upButton.gameObject.SetActive(floorNumber < numFloors);
            m_downButton.gameObject.SetActive(floorNumber > 1);
            if(arrowButtonClickCallback != null)
            {
                m_upButton.onClick.AddListener(
                    () => arrowButtonClickCallback(m_floorNumber, Elevator.Direction.UP));
                m_downButton.onClick.AddListener(
                    () => arrowButtonClickCallback(m_floorNumber, Elevator.Direction.DOWN));
            }
        }
        #endregion
    }
}