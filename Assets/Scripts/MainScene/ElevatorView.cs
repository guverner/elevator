﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ElevatorGame
{
    public class ElevatorView : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private RectTransform m_rectTransform;
        [SerializeField]
        private Image m_doorsTimerImage;
        [SerializeField]
        private GameObject m_openView;
        [SerializeField]
        private GameObject m_closedView;

        [SerializeField]
        private float m_timeOpenCloseDoors;
        [SerializeField]
        private float m_timeDoorIsOpen;
        [SerializeField]
        private float m_timePerFloor;

        private IElevator m_elevator;
        #endregion

        #region Unity Events
        private void OnDestroy()
        {
            if(m_elevator != null)
            {
                m_elevator.goToFloorEvent -= GoToFloor;
                m_elevator.stopEvent -= StopMove;
            }
        }
        #endregion

        #region Public
        public void Init(IElevator elevator)
        {
            m_elevator = elevator;

            this.transform.SetAsLastSibling();
            m_elevator.goToFloorEvent += GoToFloor;
            m_elevator.stopEvent += StopMove;
        }
        #endregion

        private void StopMove()
        {
            this.StopAllCoroutines();
        }

        private void GoToFloor(int floor)
        {
            StopAllCoroutines();
            StartCoroutine(GoToFloorCoroutine(floor));
        }

        private IEnumerator GoToFloorCoroutine(int floor)
        {
            yield return StartCoroutine(MoveCoroutine(floor));
            m_elevator.OpenDoors();
            yield return StartCoroutine(DoorsCoroutine(true));
            yield return new WaitForSeconds(m_timeDoorIsOpen);
            m_elevator.CloseDoors();
            yield return StartCoroutine(DoorsCoroutine(false));
            m_elevator.StopAtFloor(floor);
        }

        private IEnumerator MoveCoroutine(int floor)
        {
            Vector3 currentPos = m_rectTransform.localPosition;
            float startPosY = currentPos.y;
            float finishPosY = m_rectTransform.rect.height * (floor - 1);
            float timeToMove = Mathf.Abs(finishPosY - startPosY) / 
                m_rectTransform.rect.height * m_timePerFloor;

            float currentTime = 0.0f;
            while(currentTime < timeToMove)
            {
                currentPos.y = Mathf.Lerp(startPosY, finishPosY, currentTime / timeToMove);
                m_rectTransform.localPosition = currentPos;
                currentTime += Time.deltaTime;
                yield return null;
            }
        }

        private IEnumerator DoorsCoroutine(bool isOpen)
        {
            m_doorsTimerImage.gameObject.SetActive(true);

            float currentTime = 0.0f;
            while(currentTime < m_timeOpenCloseDoors)
            {
                m_doorsTimerImage.fillAmount = currentTime / m_timeOpenCloseDoors;
                currentTime += Time.deltaTime;
                yield return null;
            }

            m_doorsTimerImage.gameObject.SetActive(false);
            m_openView.SetActive(isOpen);
            m_closedView.SetActive(!isOpen);
        }
    }
}