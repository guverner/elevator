﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ElevatorGame
{
    public class ElevatorInterface : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private ScrollRect m_scrollRect;
        [SerializeField]
        private FloorButton m_floorButtonPrefab;
        [SerializeField]
        private Transform m_floorButtonsContainer;
        [SerializeField]
        private Button m_stopButton;

        private IElevator m_elevator;
        #endregion

        public void Init(IElevator elevator, int numFloors)
        {
            m_elevator = elevator;

            StartCoroutine(InitFloorButtons(numFloors));
        }

        private IEnumerator InitFloorButtons(int numFloors)
        {
            for(int i = 0; i < numFloors; i++)
            {
                FloorButton floorButton =
                    GameObject.Instantiate(m_floorButtonPrefab, m_floorButtonsContainer, false);
                floorButton.Init(i + 1, OnFloorButtonClick);
            }

            yield return null;

            CheckBlockScrollRectScroll();
        }

        #region Button Handlers
        private void OnFloorButtonClick(int floorNumber)
        {
            m_elevator.WantMoveToFloor(floorNumber);
        }

        public void OnStopClick()
        {
            m_elevator.Stop();
        }
        #endregion

        #region Helpers
        private void CheckBlockScrollRectScroll()
        {
            bool isContentFitIn = 
                m_scrollRect.viewport.rect.height >=
                m_scrollRect.content.rect.height;

            m_scrollRect.enabled = !isContentFitIn;
        }
        #endregion
    }
}