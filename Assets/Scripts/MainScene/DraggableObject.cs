﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ElevatorGame
{
    public class DraggableObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region Fields
        [SerializeField]
        private Scrollbar m_scrollbar;
        private bool m_isDraggable;
        private float m_parentYMax;
        private float m_parentYMin;
        private bool m_isDragging;
        #endregion

        public IEnumerator Init()
        {
            yield return null;

            float height = this.transform.GetComponent<RectTransform>().sizeDelta.y;

            Rect parentRect = this.transform.parent.GetComponent<RectTransform>().rect;
            m_parentYMax = parentRect.yMax - height;
            m_parentYMin = parentRect.yMin;

            m_isDraggable = height > parentRect.height;

            if(m_isDraggable)
            {
                m_scrollbar.gameObject.SetActive(true);
                m_scrollbar.size = height / parentRect.height - 1.0f;
            }
        }

        public void OnScrollbarValueChanged()
        {
            if(m_isDragging) return;
            Vector2 newPos = this.transform.localPosition;
            newPos.y = Mathf.Lerp(m_parentYMin, m_parentYMax, m_scrollbar.value);
            this.transform.localPosition = newPos;
        }

        private void SetPosition(float deltaY)
        {
            this.transform.Translate(new Vector2(0.0f, deltaY));

            Vector3 localPosition = this.transform.localPosition;

            if(localPosition.y < m_parentYMax)
                this.transform.localPosition = new Vector2(localPosition.x, m_parentYMax);
            else if(localPosition.y > m_parentYMin)
                this.transform.localPosition = new Vector2(localPosition.x, m_parentYMin);
        }

        private void SetScrollBarValue()
        {
            m_scrollbar.value = Mathf.InverseLerp(
                m_parentYMin, m_parentYMax, this.transform.localPosition.y);
        }

        #region Drag
        public void OnBeginDrag(PointerEventData eventData)
        {
            if(!m_isDraggable) return;
            m_isDragging = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if(!m_isDraggable) return;

            SetPosition(eventData.delta.y);
            SetScrollBarValue();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if(!m_isDraggable) return;
            m_isDragging = false;
        }
        #endregion
    }
}