﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ElevatorGame
{
    [RequireComponent(typeof(Button))]
    public class FloorButton : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Button m_button;
        [SerializeField]
        private Text m_floorNumberText;
        #endregion

        #region Public
        public void Init(int num, UnityAction<int> clickCallback)
        {
            this.GetComponent<Image>().alphaHitTestMinimumThreshold = 0.5f;

            m_floorNumberText.text = num.ToString();

            if(clickCallback != null)
                m_button.onClick.AddListener(() => clickCallback(num));
        }
        #endregion
    }
}