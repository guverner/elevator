﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElevatorGame
{
    public class MainSceneController : SceneController
    {
        #region Fields
        [SerializeField]
        private FloorsContainer m_floorsContainer;
        [SerializeField]
        private ElevatorView m_elevatorView;
        [SerializeField]
        private ElevatorInterface elevatorInterface;
        #endregion

        public override void Init()
        {
            IElevator elevator = new Elevator(m_elevatorView.GetComponent<RectTransform>());
            int numFloors = Game.Instance.NumFloors;

            m_floorsContainer.Init(elevator, numFloors);
            m_elevatorView.Init(elevator);
            elevatorInterface.Init(elevator, numFloors);

            Game.Instance.Elevator = elevator;
        }
    }
}