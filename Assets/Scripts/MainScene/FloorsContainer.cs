﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElevatorGame
{
    public class FloorsContainer : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Floor m_floorPrefab;
        [SerializeField]
        private Transform m_floorsContainer;
        [SerializeField]
        private DraggableObject m_draggableObj;

        private IElevator m_elevator;
        #endregion

        #region Public
        public void Init(IElevator elevator, int numFloors)
        {
            m_elevator = elevator;

            for(int i = numFloors - 1; i >= 0; i--)
            {
                Floor floor = GameObject.Instantiate(m_floorPrefab, m_floorsContainer, false);
                floor.Init(i + 1, numFloors, OnArrowButtonClick);
            }

            StartCoroutine(m_draggableObj.Init());
        }
        #endregion

        #region ButtonHandlers
        private void OnArrowButtonClick(int floor, Elevator.Direction direction)
        {
            m_elevator.WantMoveFromFloor(floor, direction);
        }
        #endregion
    }
}