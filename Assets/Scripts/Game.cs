﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ElevatorGame
{
    public class Game : UnitySingleton<Game>
    {
        public int NumFloors { get; set; }
        public IElevator Elevator { get; set; }

        #region Unity Events
        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        #endregion

        override protected void Init()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            GameObject.FindObjectOfType<SceneController>().Init();
        }
    }
}